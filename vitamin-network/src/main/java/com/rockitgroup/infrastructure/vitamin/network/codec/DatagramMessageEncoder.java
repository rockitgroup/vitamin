package com.rockitgroup.infrastructure.vitamin.network.codec;

import com.rockitgroup.infrastructure.vitamin.network.util.IOUtils;
import io.netty.buffer.ByteBuf;
import io.netty.channel.AddressedEnvelope;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;
import io.netty.handler.codec.MessageToMessageEncoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.net.InetSocketAddress;
import java.util.List;


@Slf4j
@Component
public class DatagramMessageEncoder extends MessageToMessageEncoder<AddressedEnvelope<ByteBuf, InetSocketAddress>>  {

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, AddressedEnvelope<ByteBuf, InetSocketAddress> messageAndSocketAddress, List<Object> list) throws Exception {
        ByteBuf in = messageAndSocketAddress.content();
        ByteBuf out = channelHandlerContext.alloc().buffer();

        byte cmd = IOUtils.readByte(in);
        int size = IOUtils.readInt(in);

        out.writeByte(cmd);
        if (size > 0) {
            byte[] bodyData = new byte[size];
            IOUtils.read(in, bodyData, 0, size);

            out.writeInt(bodyData.length);
            out.writeBytes(bodyData);

        } else {
            out.writeInt(0);
        }

        list.add(new DatagramPacket(out, messageAndSocketAddress.recipient()));
    }
}
