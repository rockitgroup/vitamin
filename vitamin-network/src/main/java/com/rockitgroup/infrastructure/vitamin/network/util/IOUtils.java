package com.rockitgroup.infrastructure.vitamin.network.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rockitgroup.infrastructure.vitamin.network.domain.Message;
import io.netty.buffer.ByteBuf;
import org.msgpack.jackson.dataformat.MessagePackFactory;

import java.io.EOFException;
import java.io.IOException;

public class IOUtils {

    public static Object readMessage(Message message, Class clazz) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper(new MessagePackFactory());
        int length =  message.getDis().available();
        byte[] data = new byte[length];
        message.getDis().read(data);
        return objectMapper.readValue(data, clazz);
    }

    public static Message writeMessage(Object data, byte commandId) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper(new MessagePackFactory());
        byte[] bytes = objectMapper.writeValueAsBytes(data);     // Convert object sang bytes
        Message response = new Message(commandId);
        response.getDos().write(bytes);
        return response;
    }

    public static int read(ByteBuf in) throws IOException {
        return !in.isReadable() ? -1 : in.readByte() & 255;
    }

    public static byte readByte(ByteBuf in) throws IOException {
        int ch = read(in);
        if (ch < 0)
            throw new EOFException();
        return (byte)(ch);
    }

    public static int readInt(ByteBuf in) throws IOException {
        int ch1 = read(in);
        int ch2 = read(in);
        int ch3 = read(in);
        int ch4 = read(in);
        if ((ch1 | ch2 | ch3 | ch4) < 0)
            throw new EOFException();
        return ((ch1 << 24) + (ch2 << 16) + (ch3 << 8) + (ch4 << 0));
    }

    public static int read(ByteBuf in, byte b[], int off, int len) throws IOException {
        if (b == null) {
            throw new NullPointerException();
        } else if (off < 0 || len < 0 || len > b.length - off) {
            throw new IndexOutOfBoundsException();
        } else if (len == 0) {
            return 0;
        }

        int c = read(in);
        if (c == -1) {
            return -1;
        }
        b[off] = (byte)c;

        int i = 1;
        for (; i < len ; i++) {
            c = read(in);
            if (c == -1) {
                break;
            }
            b[off + i] = (byte)c;
        }
        return i;
    }
}
