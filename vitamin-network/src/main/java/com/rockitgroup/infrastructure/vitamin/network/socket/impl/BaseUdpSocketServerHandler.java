package com.rockitgroup.infrastructure.vitamin.network.socket.impl;

import com.rockitgroup.infrastructure.vitamin.network.domain.Session;
import com.rockitgroup.infrastructure.vitamin.network.service.ChannelService;
import com.rockitgroup.infrastructure.vitamin.network.socket.UdpSocketServerHandler;
import io.netty.channel.Channel;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseUdpSocketServerHandler extends UdpSocketServerHandler {

    @Autowired
    protected ChannelService channelService;

    @Override
    public Session getSession(long sessionId) {
        return channelService.getSession(sessionId);
    }

    @Override
    public Channel getChannel() {
        return channelService.getUdpChannel();
    }
}
