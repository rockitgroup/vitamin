package com.rockitgroup.infrastructure.vitamin.network.message;

import com.rockitgroup.infrastructure.vitamin.network.domain.Message;
import com.rockitgroup.infrastructure.vitamin.network.domain.Session;
import com.rockitgroup.infrastructure.vitamin.network.exception.RequestException;
import com.rockitgroup.infrastructure.vitamin.network.handler.RequestHandler;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public class SimpleMessageExecutorRunnable implements Runnable {

    private Map<Byte, RequestHandler> requestHandlerMap;
    private Session session;
    private Message message;

    public SimpleMessageExecutorRunnable(Map<Byte, RequestHandler> requestHandlerMap, Session session, Message message) {
        this.requestHandlerMap = requestHandlerMap;
        this.session = session;
        this.message = message;
    }

    @Override
    public void run() {
        if (message != null) {
            try {
                RequestHandler requestHandler = requestHandlerMap.get(message.getCommand());
                if (requestHandler != null) {
                    requestHandler.handle(session, message);
                } else {
                    throw new RequestException("Cannot find the request handler with this command id");
                }
            } catch (RequestException e) {
                log.error(String.format("Failed to handle request of message command %d for session %s IP %s", message.getCommand(), session.getSessionId(), session.getTcpAddress().toString()), e);
            }
        }
    }
}
