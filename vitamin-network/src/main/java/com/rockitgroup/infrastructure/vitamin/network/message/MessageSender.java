package com.rockitgroup.infrastructure.vitamin.network.message;

import com.rockitgroup.infrastructure.vitamin.network.domain.Message;
import com.rockitgroup.infrastructure.vitamin.network.domain.Session;
import com.rockitgroup.infrastructure.vitamin.network.socket.TcpSocketServerHandler;
import com.rockitgroup.infrastructure.vitamin.network.socket.UdpSocketServerHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MessageSender {

	@Autowired
	private TcpSocketServerHandler tcpSocketServerHandler;

    @Autowired
    private UdpSocketServerHandler udpSocketServerHandler;

    public void sendTcpMessage(Session session, Message message) {
        tcpSocketServerHandler.send(session, message);
    }

    public void sendTcpMessages(List<Session> sessions, Message message) {
        tcpSocketServerHandler.send(sessions, message);
    }

    public void sendUdpMessage(Session session, Message message) {
        udpSocketServerHandler.send(session, message);
	}

	public void sendUdpMessages(List<Session> sessions, Message message) {
        udpSocketServerHandler.send(sessions, message);
	}
}
