package com.rockitgroup.infrastructure.vitamin.network.handler;


import com.rockitgroup.infrastructure.vitamin.network.domain.Message;
import com.rockitgroup.infrastructure.vitamin.network.domain.Session;

public interface RequestHandler {

	void handle(Session session, Message message);
}
