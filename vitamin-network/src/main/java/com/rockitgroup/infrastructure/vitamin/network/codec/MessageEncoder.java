package com.rockitgroup.infrastructure.vitamin.network.codec;

import com.google.common.primitives.Ints;
import com.rockitgroup.infrastructure.vitamin.network.domain.Message;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Slf4j
@Component
public class MessageEncoder extends MessageToByteEncoder<Message> {

	@Override
	protected void encode(ChannelHandlerContext ctx, Message message, ByteBuf out) throws Exception {
		byte[] data = message.toByteArray();
		out.writeByte(data[0]);
		int size = Ints.fromByteArray(new byte[] {data[1], data[2], data[3], data[4]});
		if (size > 0) {
			byte[] bodyData = Arrays.copyOfRange(data, Message.HEADER_BYTE_SIZE, data.length);
            out.writeInt(bodyData.length);
            out.writeBytes(bodyData);
        } else {
			out.writeInt(0);
		}
	}

}
