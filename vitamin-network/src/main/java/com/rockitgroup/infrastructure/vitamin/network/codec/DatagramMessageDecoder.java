package com.rockitgroup.infrastructure.vitamin.network.codec;

import com.google.common.primitives.Longs;
import com.rockitgroup.infrastructure.vitamin.network.domain.Message;
import com.rockitgroup.infrastructure.vitamin.network.domain.Session;
import com.rockitgroup.infrastructure.vitamin.network.service.ChannelService;
import com.rockitgroup.infrastructure.vitamin.network.util.IOUtils;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;
import io.netty.handler.codec.MessageToMessageDecoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.InetSocketAddress;
import java.util.List;

@Slf4j
@Component
public class DatagramMessageDecoder extends MessageToMessageDecoder<DatagramPacket> {

    @Autowired
    private ChannelService channelService;

    protected void decode(ChannelHandlerContext channelHandlerContext, DatagramPacket datagramPacket, List<Object> out) throws Exception {
        InetSocketAddress clientAddress = datagramPacket.sender();
        ByteBuf in = datagramPacket.content();

        if (!in.isReadable(Message.HEADER_BYTE_SIZE)) {
            return;
        }

        byte cmd = IOUtils.readByte(in);
        int size = IOUtils.readInt(in);

        Message msg = null;
        try {
            if (!in.isReadable(size) || size < 0) {
                return;
            }

            if (size < 500) {
                byte[] data = new byte[size-8];
                byte[] sessionIdBytes = new byte[8];

                IOUtils.read(in, sessionIdBytes, 0, 8);
                IOUtils.read(in, data, 0, size-8);

                msg = new Message(cmd, data);

                Long sessionId = Longs.fromByteArray(sessionIdBytes);
                msg.setSessionId(sessionId);
                msg.setClientAddress(clientAddress);

                Session session = channelService.getSession(sessionId);

                if (session != null) {
                    session.setUdpAddress(clientAddress);
                }
            }
        } catch (Exception e) {
            in.clear();
            log.error("Failed to decode datagram message", e);
        }

        if (msg != null) {
            out.add(msg);
        }
    }
}
