package com.rockitgroup.infrastructure.vitamin.network.message;

import com.rockitgroup.infrastructure.vitamin.network.domain.Message;
import com.rockitgroup.infrastructure.vitamin.network.domain.Session;

public interface MessageExecutor {

    void execute(Session session, Message message);
}
