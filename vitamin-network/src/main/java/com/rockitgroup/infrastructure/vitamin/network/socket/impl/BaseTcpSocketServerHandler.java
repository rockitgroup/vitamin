package com.rockitgroup.infrastructure.vitamin.network.socket.impl;

import com.rockitgroup.infrastructure.vitamin.network.domain.Session;
import com.rockitgroup.infrastructure.vitamin.network.service.ChannelService;
import com.rockitgroup.infrastructure.vitamin.network.socket.TcpSocketServerHandler;
import io.netty.channel.Channel;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseTcpSocketServerHandler extends TcpSocketServerHandler {

    @Autowired
    protected ChannelService channelService;

    @Override
    public Session getSession(Channel channel) {
        return channelService.getSession(channel);
    }

    @Override
    public void disconnect(Session session) {
        channelService.disconnect(session);
    }

    @Override
    public Session createSession(Channel channel) {
        return channelService.createSession(channel);
    }

    @Override
    public Channel getChannel(long sessionId) {
        return channelService.getTcpChannel(sessionId);
    }
}
