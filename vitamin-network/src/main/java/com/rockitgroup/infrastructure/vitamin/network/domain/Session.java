package com.rockitgroup.infrastructure.vitamin.network.domain;

import lombok.Getter;
import lombok.Setter;

import java.net.InetSocketAddress;

@Getter
@Setter
public class Session {

    private long sessionId;
    private InetSocketAddress tcpAddress;
    private InetSocketAddress udpAddress;
    private long accountId = -1;
    private boolean isConnected;

    public void disconnect() {
        this.isConnected = false;
        this.accountId = -1;
    }
}
