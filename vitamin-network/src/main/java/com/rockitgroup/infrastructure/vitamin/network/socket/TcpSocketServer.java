package com.rockitgroup.infrastructure.vitamin.network.socket;

import com.rockitgroup.infrastructure.vitamin.network.codec.MessageDecoder;
import com.rockitgroup.infrastructure.vitamin.network.codec.MessageEncoder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.util.concurrent.GenericFutureListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.net.SocketAddress;

@Slf4j
@Component
public class TcpSocketServer {

    @Value("${tcpSocketServer.soKeepAlive}")
    private boolean soKeepAlive;

    @Value("${tcpSocketServer.connectTimeoutMillis}")
    private int connectTimeoutMillis;

    @Value("${tcpSocketServer.soBackLog}")
    private int soBackLog;

    @Value("${tcpSocketServer.tcpNoDelay}")
    private boolean tcpNoDelay;

    @Value("${tcpSocketServer.bossGroupThreadSize}")
    private int bossGroupThreadSize;

    @Value("${tcpSocketServer.workerGroupThreadSize}")
    private int workerGroupThreadSize;

    @Value("${tcpSocketServer.ipAddress}")
    private String ipAddress;

    @Value("${tcpSocketServer.port}")
    private int port;

    @Autowired
    private TcpSocketServerHandler socketServerHandler;

    @Autowired
    private MessageEncoder messageEncoder;

    @Autowired
    private MessageDecoder messageDecoder;

    @Async
    public void startAsync() {
        start();
    }

	public void start() {
        EventLoopGroup bossGroup = new NioEventLoopGroup(bossGroupThreadSize);
        EventLoopGroup workerGroup = new NioEventLoopGroup(workerGroupThreadSize);

        try {
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {

                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast(new LoggingHandler(LogLevel.INFO));
//						    ch.pipeline().addLast(new IdleStateHandler(Constants.MAX_READ_IDLE_SECONDS, Constants.MAX_WRITE_IDLE_SECONDS, Constants.MAX_ALL_IDLE_SECONDS));
                            ch.pipeline().addLast(messageDecoder);
                            ch.pipeline().addLast(messageEncoder);
                            ch.pipeline().addLast(socketServerHandler);
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, soBackLog)
                    .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, connectTimeoutMillis)
                    .childOption(ChannelOption.TCP_NODELAY, tcpNoDelay)
                    .childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
                    .childOption(ChannelOption.SO_KEEPALIVE, soKeepAlive);

            ChannelFuture future;
            if (ipAddress != null) {
                future = bootstrap.bind(ipAddress, port).sync();
            } else {
                future = bootstrap.bind(port).sync();
            }

            future.addListener(new GenericFutureListener<ChannelFuture>() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    if (future.isSuccess()) {
                        SocketAddress localAddress = future.channel().localAddress();
                        log.info("RELIABLE SERVER STARTED AT:" + localAddress.toString());
                    } else {
                        log.error("Bound attempt failed! ", future.cause());
                    }
                }
            });

            future.channel().closeFuture().sync();
        } catch (Exception e) {
            log.error("Failed to start tcp server", e);
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
	}
}
