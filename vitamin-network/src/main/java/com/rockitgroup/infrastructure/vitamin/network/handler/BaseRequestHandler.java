package com.rockitgroup.infrastructure.vitamin.network.handler;

import com.rockitgroup.infrastructure.vitamin.network.domain.Message;
import com.rockitgroup.infrastructure.vitamin.network.domain.Session;
import com.rockitgroup.infrastructure.vitamin.network.message.MessageSender;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 12/9/18
 * Time: 1:34 PM
 */
public abstract class BaseRequestHandler implements RequestHandler {

    @Autowired
    private MessageSender messageSender;

    public void sendTcpMessage(Session session, Message message) {
        messageSender.sendTcpMessage(session, message);
    }

    public void sendTcpMessages(List<Session> sessions, Message message) {
        messageSender.sendTcpMessages(sessions, message);
    }

    public void sendUdpMessage(Session session, Message message) {
        messageSender.sendUdpMessage(session, message);
    }

    public void sendUdpMessages(List<Session> sessions, Message message) {
        messageSender.sendUdpMessages(sessions, message);
    }
}
