package com.rockitgroup.infrastructure.vitamin.network.codec;

import com.rockitgroup.infrastructure.vitamin.network.domain.Message;
import com.rockitgroup.infrastructure.vitamin.network.util.IOUtils;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Slf4j
@Component
public class MessageDecoder extends ByteToMessageDecoder {

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		if (!in.isReadable(Message.HEADER_BYTE_SIZE)) {
			return;
		}

		Message msg;
		try {
			byte cmd = IOUtils.readByte(in);
			int size = IOUtils.readInt(in);

			if (!in.isReadable(size) || size < 0) {
				handleBadMessage(in);
				return;
			}

			if (size > 10000) {
				throw new IOException("Data too big");
			}

			byte[] data = new byte[size];

			int byteRead = 0;
			while (byteRead != -1 && byteRead < size) {
				int len = IOUtils.read(in, data, byteRead, size - byteRead);
				if (len > 0) {
					byteRead += len;
				}
				if (len == -1) {
					return;
				}
			}

            msg = new Message(cmd, data);
        } catch (Exception e) {
			in.clear();
			throw new RuntimeException(e);
		}

        out.add(msg);

    }

	private void handleBadMessage(ByteBuf in) {
		while (in.isReadable()) {
			in.readByte();
		}
	}
}
