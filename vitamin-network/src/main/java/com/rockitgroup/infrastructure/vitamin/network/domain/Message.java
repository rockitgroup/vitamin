package com.rockitgroup.infrastructure.vitamin.network.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.InetSocketAddress;

@Slf4j
@Getter
@Setter
public class Message {

    public static final int HEADER_BYTE_SIZE = 5;

    private long sessionId;
    private byte command;
    private InetSocketAddress clientAddress = null;
    private ByteArrayOutputStream os = null;
    private DataOutputStream dos = null;
    private ByteArrayInputStream is = null;
    private DataInputStream dis = null;
    private byte[] bytes = null;

    public Message(byte command) {
        this.command = command;
        os = new ByteArrayOutputStream();
        dos = new DataOutputStream(os);
    }

    public Message(byte command, byte[] data) {
        this.command = command;
        is = new ByteArrayInputStream(data);
        dis = new DataInputStream(is);
    }

    public byte[] toByteArray() {
        int dataLength = 0;
        byte[] data = null;
        if (bytes == null) {
            try {
                if (dos != null) {
                    dos.flush();
                    data = os.toByteArray();
                    dataLength = data.length;
                    dos.close();
                }
                ByteArrayOutputStream bos1 = new ByteArrayOutputStream(HEADER_BYTE_SIZE + dataLength);
                DataOutputStream dos1 = new DataOutputStream(bos1);
                dos1.writeByte(command);
                dos1.writeInt(dataLength);
                if (dataLength > 0) {
                    dos1.write(data);
                }
                bytes = bos1.toByteArray();
                dos1.close();
            } catch (IOException e) {
                log.error("Failed to convert to byte array", e);
            }
        }
        return bytes;
    }

    public void cleanUp() {
        try {
            if (dis != null) {
                dis.close();
            }
            if (dos != null) {
                dos.close();
            }
            bytes = null;
        } catch (IOException e) {
            log.error("Error cleanUp():", e);
        }
    }

}
