package com.rockitgroup.infrastructure.vitamin.network.service;

import com.rockitgroup.infrastructure.vitamin.network.domain.Session;
import io.netty.channel.Channel;
import io.netty.util.AttributeKey;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.net.InetSocketAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class ChannelService {

	private static final AttributeKey<Session> SESSION = AttributeKey.valueOf("dispatcher.session");

	private Map<Long, Channel> channelMap = new ConcurrentHashMap<>();
	private Map<Long, Session> sessionIdMap = new ConcurrentHashMap<>();

    private AtomicLong idCounter = new AtomicLong(1);

	@Getter
	@Setter
	private Channel udpChannel;

	public Session createSession(Channel channel) {
		InetSocketAddress remoteAddress = (InetSocketAddress) channel.remoteAddress();

        long sessionId = idCounter.getAndIncrement();
		Session session = new Session();
		session.setSessionId(sessionId);
		session.setTcpAddress(remoteAddress);
		session.setConnected(true);
		channel.attr(SESSION).set(session);
		channelMap.put(sessionId, channel);

        saveSession(session);

		return session;
	}

    public void saveSession(Session session) {
        if (session != null) {
            sessionIdMap.put(session.getSessionId(), session);
        }
    }

	public Channel getTcpChannel(long sessionId) {
		return channelMap.get(sessionId);
	}

	public void disconnect(Session session) {
		if (session != null) {
			Channel channel = getTcpChannel(session.getSessionId());
			if (channel != null) {
				channel.disconnect();
			}
			channelMap.remove(session.getSessionId());
			sessionIdMap.remove(session.getSessionId());
			session.disconnect();
		}
	}

	public Session getSession(Channel channel) {
		return channel.attr(SESSION).get();
	}

	public Session getSession(Long sessionId) {
        if (sessionId == null || sessionId < 0) {
            return null;
        }
		return sessionIdMap.get(sessionId);
	}
}
