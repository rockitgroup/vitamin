package com.rockitgroup.infrastructure.vitamin.network.socket;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.rockitgroup.infrastructure.vitamin.network.domain.Message;
import com.rockitgroup.infrastructure.vitamin.network.domain.Session;
import com.rockitgroup.infrastructure.vitamin.network.message.MessageExecutor;
import io.netty.channel.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;

import static com.codahale.metrics.MetricRegistry.name;

@Slf4j
public abstract class TcpSocketServerHandler extends SimpleChannelInboundHandler<Message> {

    protected MetricRegistry metrics = new MetricRegistry();

    @Value("${tcpSocketServer.messageExecutionTimerName:tcpRequestHandler}")
    private String messageExecutionTimerName;

    @Autowired
    @Qualifier("tcpMessageExecutor")
    private MessageExecutor messageExecutor;

    @Override
	public void channelActive(final ChannelHandlerContext ctx) throws Exception {
		Channel channel = ctx.channel();
		Session session = createSession(channel);

		handleConnectedRequest(session);

        log.info(String.format("[INFO] [CLIENT] - %s connected - sessionId: %s", session.getTcpAddress(), session.getSessionId()));
	}


	@Override
	protected void channelRead0(final ChannelHandlerContext ctx, final Message message) throws Exception {
        if (message == null) {
            return;
        }

		Session session = getSession(ctx.channel());
		if (session != null) {
			Timer handleRequestTimer = metrics.timer(name(TcpSocketServerHandler.class, messageExecutionTimerName));
			Timer.Context context = handleRequestTimer.time();
            preHandleMessage(session, message);
            messageExecutor.execute(session, message);
            postHandleMessage(session, message);
			context.stop();
		}
	}

    public void send(Session session, Message message) {
        Channel channel = getChannel(session.getSessionId());
        if (channel != null) {
            ChannelFuture future = channel.writeAndFlush(message);
            future.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    if (future.isSuccess()) {
                        log.debug("[DEBUG] [RESPONSE]\n" + message.toString());
                    }
                }
            });
        }
    }

    public void send(List<Session> receivers, final Message message) {
        for (Session receiver : receivers) {
            send(receiver, message);
        }
    }

    @Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		log.error("Exception happened in socket handler", cause);
		ctx.close();
	}


	@Override
	public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
		Channel channel = ctx.channel();
		Session session = getSession(channel);

		handleDisconnectedRequest(session);

        log.info(String.format("[INFO] [CLIENT] - %s disconnected - sessionId: %s", session.getTcpAddress(), session.getSessionId()));

		disconnect(session);
	}

    public abstract void preHandleMessage(Session session, Message message);

    public abstract void postHandleMessage(Session session, Message message);

	public abstract void handleConnectedRequest(Session session);

    public abstract void handleDisconnectedRequest(Session session);

    public abstract Session getSession(Channel channel);

    public abstract void disconnect(Session session);

    public abstract Session createSession(Channel channel);

    public abstract Channel getChannel(long sessionId);

}
