package com.rockitgroup.infrastructure.vitamin.network.message;

import com.rockitgroup.infrastructure.vitamin.network.domain.Message;
import com.rockitgroup.infrastructure.vitamin.network.domain.Session;
import com.rockitgroup.infrastructure.vitamin.network.handler.RequestHandler;
import org.springframework.core.task.TaskExecutor;

import java.util.Map;

public class SimpleMessageExecutor implements MessageExecutor {

    private static final String MESSAGE_EXECUTOR_THREAD_NAME_TEMPLATE = "message-executor-session-%s-command-%s";

    private TaskExecutor taskExecutor;

    private Map<Byte, RequestHandler> requestHandlerMap;

    public SimpleMessageExecutor(Map<Byte, RequestHandler> requestHandlerMap, TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
        this.requestHandlerMap = requestHandlerMap;
    }

    @Override
    public void execute(Session session,Message message) {
        String threadName = String.format(MESSAGE_EXECUTOR_THREAD_NAME_TEMPLATE, session.getSessionId(), message.getCommand());
        Runnable messageExecutorRunnable = new SimpleMessageExecutorRunnable(requestHandlerMap, session, message);
        taskExecutor.execute(new Thread(messageExecutorRunnable, threadName));
    }
}
