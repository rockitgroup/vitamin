package com.rockitgroup.infrastructure.vitamin.network.socket;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.rockitgroup.infrastructure.vitamin.network.domain.Message;
import com.rockitgroup.infrastructure.vitamin.network.domain.Session;
import com.rockitgroup.infrastructure.vitamin.network.message.MessageExecutor;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.socket.DatagramPacket;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;

import static com.codahale.metrics.MetricRegistry.name;

@Slf4j
public abstract class UdpSocketServerHandler extends SimpleChannelInboundHandler<Message> {

    protected MetricRegistry metrics = new MetricRegistry();

    @Value("${udpSocketServer.messageExecutionTimerName:udpRequestHandler}")
    private String messageExecutionTimerName;

    @Autowired
    @Qualifier("udpMessageExecutor")
    private MessageExecutor messageExecutor;

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Message message) throws Exception {
        if (message == null) {
            return;
        }

        Session session = getSession(message.getSessionId());
        if (session != null) {
            Timer handleRequestTimer = metrics.timer(name(UdpSocketServerHandler.class, messageExecutionTimerName));
            Timer.Context context = handleRequestTimer.time();
            preHandleMessage(session, message);
            messageExecutor.execute(session, message);
            postHandleMessage(session, message);
            context.stop();
        }
    }


    public void send(Session session, Message message) {
        Channel channel = getChannel();
        if (channel != null) {
            ChannelFuture future = channel.writeAndFlush(new DatagramPacket(Unpooled.wrappedBuffer(message.toByteArray()), session.getUdpAddress()));
            future.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    if (future.isSuccess()) {
                        log.debug("[DEBUG] [RESPONSE]\n" + message.toString());
                    }
                }
            });
        }
    }

    public void send(List<Session> receivers, final Message message) {
        for (Session receiver : receivers) {
            send(receiver, message);
        }
    }

    public abstract void preHandleMessage(Session session, Message message);

    public abstract void postHandleMessage(Session session, Message message);

    public abstract Session getSession(long sessionId);

    public abstract Channel getChannel();
}
