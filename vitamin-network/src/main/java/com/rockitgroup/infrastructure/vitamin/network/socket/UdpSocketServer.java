package com.rockitgroup.infrastructure.vitamin.network.socket;

import com.rockitgroup.infrastructure.vitamin.network.codec.DatagramMessageDecoder;
import com.rockitgroup.infrastructure.vitamin.network.codec.DatagramMessageEncoder;
import com.rockitgroup.infrastructure.vitamin.network.service.ChannelService;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.DatagramChannel;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.util.concurrent.GenericFutureListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class UdpSocketServer {

    @Value("${udpSocketServer.workerGroupThreadSize}")
    private int workerGroupThreadSize;

    @Value("${udpSocketServer.port}")
    private int port;

    @Autowired
    private UdpSocketServerHandler udpSocketServerHandler;

    @Autowired
    private ChannelService channelService;

    @Autowired
    private DatagramMessageDecoder datagramMessageDecoder;

    @Autowired
    private DatagramMessageEncoder datagramMessageEncoder;

    @Async
    public void startAsync() {
        start();
    }

    public void start() {
        EventLoopGroup workerGroup = new NioEventLoopGroup(workerGroupThreadSize);
        try {
            Channel channel = createChannel(workerGroup);
            channelService.setUdpChannel(channel);
            channel.closeFuture().await();
        } catch (Exception e) {
            log.error("Failed to start reliable server", e);
        } finally {
            workerGroup.shutdownGracefully();
        }
    }

    private Channel createChannel(EventLoopGroup workerGroup) throws InterruptedException {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(workerGroup)
                .channel(NioDatagramChannel.class)
                .option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
                .handler(new ChannelInitializer<DatagramChannel>() {
                    @Override
                    public void initChannel(DatagramChannel ch) throws Exception {
                        ch.pipeline().addLast(datagramMessageDecoder);
                        ch.pipeline().addLast(datagramMessageEncoder);
                        ch.pipeline().addLast(udpSocketServerHandler);
                    }
                });

        ChannelFuture future = bootstrap.bind(port).sync();
        future.addListener(new GenericFutureListener<ChannelFuture>() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                if (future.isSuccess()) {
                    log.info("FAST SERVER STARTED AT:" + port);
                } else {
                    log.error("Bound attempt failed! ", future.cause());
                }
            }
        });

        return future.channel();
    }

}
