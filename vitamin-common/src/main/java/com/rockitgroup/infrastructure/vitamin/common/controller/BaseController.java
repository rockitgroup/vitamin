package com.rockitgroup.infrastructure.vitamin.common.controller;

import com.rockitgroup.infrastructure.vitamin.common.dto.ErrorDTO;
import com.rockitgroup.infrastructure.vitamin.common.dto.ResponseDTO;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.DTOMapper;
import com.rockitgroup.infrastructure.vitamin.common.exception.BadRequestException;
import com.rockitgroup.infrastructure.vitamin.common.exception.NotFoundException;
import com.rockitgroup.infrastructure.vitamin.common.exception.UnauthorizedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
public abstract class BaseController {

    @Autowired
    private DTOMapper dtoMapper;

    public ResponseDTO generateFailResponse(ResponseDTO responseDTO, int httpCode, String errorMessage) {
        responseDTO.setSuccess(false);
        responseDTO.setStatus(httpCode);
        responseDTO.setError(new ErrorDTO(httpCode, errorMessage));
        return responseDTO;
    }

    public ResponseDTO generateFailResponse(HttpServletRequest servletRequest, ResponseDTO responseDTO, Throwable e) {
        int httpCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
        if (e instanceof NotFoundException) {
            httpCode = HttpStatus.NOT_FOUND.value();
        } else if (e instanceof BadRequestException) {
            httpCode = HttpStatus.BAD_REQUEST.value();
        } else if (e instanceof UnauthorizedException) {
            httpCode = HttpStatus.UNAUTHORIZED.value();
        }

        log.error(String.format("Failed response for request %s of IP address %s with http status %d", servletRequest.getRequestURL(), servletRequest.getRemoteAddr(), httpCode), e);

        responseDTO.setSuccess(false);
        responseDTO.setStatus(httpCode);
        responseDTO.setError(new ErrorDTO(httpCode, e.getMessage()));
        return responseDTO;
    }

    public ResponseDTO generateSuccessResponse(ResponseDTO responseDTO) {
        responseDTO.setSuccess(true);
        responseDTO.setStatus(HttpStatus.OK.value());
        return responseDTO;
    }

    public ResponseDTO generateSuccessResponse(ResponseDTO responseDTO, List objects) {
        responseDTO = generateSuccessResponse(responseDTO);
        responseDTO.setObject(objects);
        return responseDTO;
    }

    public ResponseDTO generateSuccessResponse(ResponseDTO responseDTO, Object objects) {
        responseDTO = generateSuccessResponse(responseDTO);
        responseDTO.setObject(objects);
        return responseDTO;
    }

    public ResponseDTO generateSuccessResponse(ResponseDTO responseDTO, List objects, Class clazz) {
        responseDTO = generateSuccessResponse(responseDTO);
        responseDTO.setObject(dtoMapper.map(objects, clazz));
        return responseDTO;
    }

    public ResponseDTO generateSuccessResponse(ResponseDTO responseDTO, Object object, Class clazz) {
        responseDTO = generateSuccessResponse(responseDTO);
        responseDTO.setObject(dtoMapper.map(object, clazz));
        return responseDTO;
    }

}
