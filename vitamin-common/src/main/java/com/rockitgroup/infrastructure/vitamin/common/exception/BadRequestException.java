package com.rockitgroup.infrastructure.vitamin.common.exception;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 11/14/18
 * Time: 3:56 PM
 */
public class BadRequestException extends IllegalArgumentException {

    public BadRequestException() {
        super();
    }

    public BadRequestException(String s) {
        super(s);
    }

    public BadRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadRequestException(Throwable cause) {
        super(cause);
    }
}
