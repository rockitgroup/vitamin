package com.rockitgroup.infrastructure.vitamin.common.dto.mapper;

import com.google.common.base.Optional;
import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.metadata.Type;

public class OptionalConverter extends CustomConverter<Optional, Object> {

	private MapperFacade mapper;

	private MapperFacade getMapperFacade() {
		if (mapper == null) {
			mapper = MapperFacadeFactory.create();
		}
		return mapper;
	}

	@Override
	public boolean canConvert(Type<?> sourceType, Type<?> destinationType) {
		return this.sourceType.isAssignableFrom(sourceType);
	}

    @Override
    public Object convert(Optional source, Type<?> destinationType, MappingContext mappingContext) {
        return getMapperFacade().map(source.orNull(), destinationType.getRawType());
    }

}
