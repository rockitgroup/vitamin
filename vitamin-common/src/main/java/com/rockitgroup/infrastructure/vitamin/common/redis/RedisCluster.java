package com.rockitgroup.infrastructure.vitamin.common.redis;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisSlotBasedConnectionHandler;
import redis.clients.jedis.exceptions.JedisNoReachableClusterNodeException;
import redis.clients.util.JedisClusterCRC16;

import java.util.Set;

public final class RedisCluster extends JedisCluster {

    public RedisCluster(Set<HostAndPort> nodes, final GenericObjectPoolConfig poolConfig) {
        super(nodes, poolConfig);
    }

    public Jedis getJedis(String key) {
        if (connectionHandler != null) {
            JedisSlotBasedConnectionHandler handler = (JedisSlotBasedConnectionHandler) connectionHandler;
            return handler.getConnectionFromSlot(JedisClusterCRC16.getSlot(key));
        }
        throw new JedisNoReachableClusterNodeException("No reachable node in cluster");
    }

    public void renewSlotCache() {
        connectionHandler.renewSlotCache();
    }

}
