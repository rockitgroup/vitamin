package com.rockitgroup.infrastructure.vitamin.common.util;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.Calendar;
import java.util.Date;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 9/21/18
 * Time: 1:17 AM
 */
public class DateUtils extends org.apache.commons.lang3.time.DateUtils {

    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String FULL_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static String formatDate(DateTime dateTime) {
        return formatDateTime(dateTime, DATE_FORMAT);
    }

    public static String formatFullDateTime(DateTime dateTime) {
        return formatDateTime(dateTime, FULL_DATETIME_FORMAT);
    }

    public static String formatDateTime(DateTime dateTime, String pattern) {
        return DateTimeFormat.forPattern(pattern).print(dateTime);
    }

    public static DateTime parseDate(String dateTime) {
        return parseDateTime(dateTime, DATE_FORMAT);
    }

    public static DateTime parseDateTime(String dateTime, String pattern) {
        if (StringUtils.isEmpty(dateTime)) {
            return null;
        }
        return DateTimeFormat.forPattern(pattern).parseDateTime(dateTime);
    }

    public static DateTime addOneDay(String dateStr) {
        return addOneDay(DateUtils.parseDate(dateStr));
    }

    public static DateTime addOneDay(DateTime dateTime) {
        if (dateTime != null) {
            dateTime = dateTime.plusDays(1);
        }
        return dateTime;
    }

    public static DateTime minusOneDay(String dateStr) {
        return minusOneDay(DateUtils.parseDate(dateStr));
    }

    public static DateTime minusOneDay(DateTime dateTime) {
        if (dateTime != null) {
            dateTime = dateTime.minusDays(1);
        }
        return dateTime;
    }

    public static Date getLastDateOfQuarter(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)/3 * 3 + 2);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));

        return cal.getTime();
    }

    public static DateTime getLastDateTimeOfQuarter(Date date){
        Date lastDate = DateUtils.getLastDateOfQuarter(date);
        return DateUtils.getEndTimeOfDate(lastDate);
    }
    public static DateTime getEndTimeOfDate(Date date){
        return new DateTime(date).withTime(23,59,59,999);
    }



}
