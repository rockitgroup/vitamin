package com.rockitgroup.infrastructure.vitamin.common.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseDTO {
    private boolean success;
    private int status;
    private ErrorDTO error;
    private Object object;
}
