package com.rockitgroup.infrastructure.vitamin.common.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.List;

@Getter
@SuppressWarnings("PMD.UnusedPrivateField")
public class PageResponse<T> {
	private final int number;
	@Setter
	private int totalPages;
	@Setter
	private long totalElements;
	private final List<T> content;

	public PageResponse(Page<T> pageModel) {
		this(pageModel.getNumber(), pageModel.getTotalPages(), pageModel.getTotalElements(), pageModel.getContent());
	}

	public PageResponse(int number, int totalPages, long totalElements, List<T> content) {
		this.number = number;
		this.totalElements = totalElements;
		this.totalPages = totalPages;
		this.content = content;
	}

	public PageResponse() {
		this.number = 0;
		this.content = null;
	}
}
