package com.rockitgroup.infrastructure.vitamin.common.exception;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 11/14/18
 * Time: 3:56 PM
 */
public class NotFoundException extends RuntimeException {

    public NotFoundException() {
        super();
    }

    public NotFoundException(String s) {
        super(s);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotFoundException(Throwable cause) {
        super(cause);
    }

}
