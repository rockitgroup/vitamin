package com.rockitgroup.infrastructure.vitamin.common.dto;

import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

@Getter
@Setter
public class BaseDTO {
    private Long id;
    private DateTime createdAt;
    private DateTime updatedAt;
    private Long createdById;
    private Long updatedById;
}
