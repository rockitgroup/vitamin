package com.rockitgroup.infrastructure.vitamin.common.configuration.interceptor;

import com.rockitgroup.infrastructure.vitamin.common.util.WebUtils;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.springframework.util.StopWatch;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class ApiTrackingInterceptor extends HandlerInterceptorAdapter {

    public static final String API_TRACKING_MARKER_NAME = "API_TRACKING";
    public static final Marker API_TRACKING_MARKER = MarkerFactory.getMarker(API_TRACKING_MARKER_NAME);

    public static final String RESPONSE_TIME = "res.timeMillis";

    public static final String STOPWATCH_REQUEST_ATTR_NAME = ApiTrackingInterceptor.class.getCanonicalName() + ".STOPWATCH";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        request.setAttribute(STOPWATCH_REQUEST_ATTR_NAME, stopWatch);

        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

        StopWatch stopWatch = (StopWatch) request.getAttribute(STOPWATCH_REQUEST_ATTR_NAME);

        if (stopWatch != null) {

            stopWatch.stop();
            loggingApiTracking(request, stopWatch.getLastTaskTimeMillis());
        }
    }

    void loggingApiTracking(HttpServletRequest request, long responseTime) {

        MDC.put(RESPONSE_TIME, String.valueOf(responseTime));

        log.info(API_TRACKING_MARKER, "{} {} ({}ms)", request.getMethod(), WebUtils.getRequestURLWithQueryString(request),
                responseTime);
    }
}
