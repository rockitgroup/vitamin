package com.rockitgroup.infrastructure.vitamin.common.redis;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.*;
import redis.clients.jedis.exceptions.JedisMovedDataException;

import java.util.*;

@Slf4j
@Component
public class RedisServer implements AutoCloseable {

    public static final String STATUS_OK = "OK";
    public static final String FORMAT_KEY = "%s.{%s}.main";
    public static final String FORMAT_HISTORY_KEY = "%s.{%s}.history";
    public static final String FORMAT_TEMP_KEY = "%s.{%s}.%s.%s";
    public static final String FORMAT_LOCK_KEY = "%s.%s.lock";
    public static final String SCOPE_NEW = "new";
    public static final String SCOPE_UNION = "union";
    private static final int MIN_RANDOM_THRESHOLD = 10;
    private static final Random random = new Random();
    public static final Long DEFAULT_SIZE = Long.valueOf(0);
    public static final double DEFAULT_SCORE = 1.0;

    private final RedisConfiguration configuration;
    private final JedisPoolConfig poolConfig;
    private final RedisCluster jedisCluster;

    @Autowired
    public RedisServer(RedisConfiguration configuration) {
        this.configuration = configuration;
        this.poolConfig = buildPoolConfig(configuration);
        this.jedisCluster = new RedisCluster(configuration.getClusterHostAndPorts(), this.poolConfig);
    }

    @Override
    public void close() throws Exception {
        jedisCluster.close();
    }

    public static RedisServer create(RedisConfiguration configuration) {
        return new RedisServer(configuration);
    }

    private JedisPoolConfig buildPoolConfig(RedisConfiguration configuration) {
        final JedisPoolConfig poolConfig = new JedisPoolConfig();

        poolConfig.setMaxTotal(configuration.getPoolConfigMaxTotal());
        poolConfig.setMaxIdle(configuration.getPoolConfigMaxIdle());
        poolConfig.setMinIdle(configuration.getPoolConfigMinIdle());
        poolConfig.setTestOnBorrow(configuration.getPoolConfigTestOnBorrow());
        poolConfig.setTestOnReturn(configuration.getPoolConfigTestOnReturn());
        poolConfig.setTestWhileIdle(configuration.getPoolConfigTestWhileIdle());
        poolConfig.setMinEvictableIdleTimeMillis(configuration.getPoolConfigMinEvictableIdleTimeInMS());
        poolConfig.setTimeBetweenEvictionRunsMillis(configuration.getPoolConfigTimeBetweenEvictionRunsInMS());
        poolConfig.setNumTestsPerEvictionRun(configuration.getPoolConfigNumTestsPerEvictionRun());
        poolConfig.setBlockWhenExhausted(configuration.getPoolConfigBlockWhenExhausted());

        return poolConfig;
    }

    public Long sortedSetAdd(String keyName, Map<String, Double> scoreMemberMap, int timeoutInSec) {
        final String fullKeyName = getKeyName(keyName);
        if (scoreMemberMap == null || scoreMemberMap.isEmpty()) {
            return DEFAULT_SIZE;
        }
        sortedSetAddInternal(fullKeyName, scoreMemberMap, timeoutInSec);
        return jedisCluster.zcard(fullKeyName);

    }

    public Long expire(final String key, final int timeoutInSec) {
        return jedisCluster.expire(key, timeoutInSec);
    }

    /**
     * Pushes a list of Long values to cache
     * if the key already exists, then it appends
     *
     * @param keyName
     * @param list
     * @param timeoutInSec
     * @return
     */
    public Long listPushLong(String keyName, List<Long> list, boolean append, int timeoutInSec) {
        if (list == null || list.isEmpty()) {
            return DEFAULT_SIZE;
        }
        final List<String> data = Lists.transform(list, number -> String.valueOf(number));
        return listPush(keyName, data, append, timeoutInSec);
    }

    /**
     * Retrieves all the items of the list from cache
     *
     * @param keyName
     * @return
     */
    public List<Long> listRangeLong(String keyName) {
        final List<String> list = listRange(keyName, 0, -1);
        if(list == null || list.isEmpty()) {
            return Collections.EMPTY_LIST;
        }

        final List<Long> data = Lists.transform(list, number -> Long.valueOf(number));
        return data;
    }

    /**
     * Retrieves elements by range - index starts with 0 and -1 index means the last element
     *
     * @param keyName
     * @param start
     * @param end
     * @return
     */
    public List<Long> listRangeLong(String keyName, long start, long end) {
        final List<String> list = listRange(keyName, start, end);
        final List<Long> data = Lists.transform(list, number -> Long.valueOf(number));
        return data;
    }

    /**
     * Pushes a list of String data to cache
     *
     * @param keyName
     * @param list
     * @param timeoutInSec
     * @return
     */
    public Long listPush(String keyName, List<String> list, boolean append, int timeoutInSec) {
        if (list == null || list.isEmpty()) {
            return DEFAULT_SIZE;
        }
        final String fullKeyName = getKeyName(keyName);
        if(!append) { // remove the key first
            jedisCluster.del(fullKeyName);
        }
        listPushInternal(fullKeyName, list, timeoutInSec);
        return jedisCluster.llen(fullKeyName);
    }

    /**
     * Retrieves elements by range - index starts with 0 and -1 index means the last element
     *
     * @param keyName
     * @param start
     * @param end
     * @return
     */
    public List<String> listRange(String keyName, long start, long end) {
        final String fullKeyName = getKeyName(keyName);
        return listRangeInternal(fullKeyName, start, end);
    }

    /**
     * This method removes any member that already exist in the "history" list. If there is no history key
     * exists, then it will simply add in the key
     *
     * @param keyName
     * @param scoreMemberMap
     * @param timeoutInSec
     * @return
     */
    public Long sortedSetMerge(String keyName, Map<String, Double> scoreMemberMap, int timeoutInSec, boolean forceReset) {
        final String fullKeyName = getKeyName(keyName);
        final String historyKeyName = getHistoryKeyName(keyName);

        if (scoreMemberMap == null || scoreMemberMap.isEmpty()) {
            if (forceReset) {
                jedisCluster.del(fullKeyName);
                jedisCluster.del(historyKeyName);
                return DEFAULT_SIZE;
            }
            return jedisCluster.zcard(fullKeyName);
        }

        final Long currentSize = jedisCluster.zcard(fullKeyName);
        if (currentSize == null || currentSize == 0) { // simply add
            sortedSetAddInternal(fullKeyName, scoreMemberMap, timeoutInSec);
            return jedisCluster.zcard(fullKeyName);
        }

        // check the history key
        final Long historySize = jedisCluster.zcard(historyKeyName);
        if (historySize == null || historySize == 0) { // simply add
            sortedSetAddInternal(fullKeyName, scoreMemberMap, timeoutInSec);
            return jedisCluster.zcard(fullKeyName);
        }

        // add the new items in a temp key
        final String newKeyName = getTempKeyName(keyName, SCOPE_NEW);
        sortedSetAddInternal(newKeyName, scoreMemberMap, timeoutInSec);
        if (forceReset) {
            final String status = jedisCluster.rename(newKeyName, fullKeyName);
            if (isSuccess(status)) {
                jedisCluster.del(historyKeyName);
                return jedisCluster.zcard(fullKeyName);
            }
            log.warn("Force reset failed for key {}, continuing with merge...", fullKeyName);
        }

        final String unionKeyName = getTempKeyName(keyName, SCOPE_UNION);
        // union all three sets with weights 1, 0, 0 with MIN aggregation
        // ZUNIONSTORE unionMembersKeyName 3 newMembersKeyName, fullKeyName, historyKeyName WEIGHTS 1 -1 -1 AGGREGATE MIN
        final ZParams params = new ZParams();
        params.weightsByDouble(1.0, 0.0, 0.0);
        params.aggregate(ZParams.Aggregate.MIN);
        jedisCluster.zunionstore(unionKeyName, params, newKeyName, fullKeyName, historyKeyName);

        // remove all the members with scores between 0.0 and 0.0
        // ZREMRANGEBYSCORE unionMembersKeyName 0.0 0.0 WITHSCORES
        jedisCluster.zremrangeByScore(unionKeyName, 0.0, 0.0);

        // union the members with the original key
        final ZParams newParams = new ZParams();
        newParams.weightsByDouble(1.0, 1.0);
        newParams.aggregate(ZParams.Aggregate.MAX);
        jedisCluster.zunionstore(fullKeyName, newParams, fullKeyName, unionKeyName);

        // cleanup
        jedisCluster.del(newKeyName);
        jedisCluster.del(unionKeyName);

        // extend cache timeout
        jedisCluster.expire(fullKeyName, timeoutInSec);
        return jedisCluster.zcard(fullKeyName);
    }

    public Long sortedSetGetSize(String keyName) {
        final String fullKeyName = getKeyName(keyName);
        return jedisCluster.zcard(fullKeyName);
    }

    public Double sortedSetGetScore(String keyName, String member) {
        final String fullKeyName = getKeyName(keyName);
        return jedisCluster.zscore(fullKeyName, member);
    }

    public Long sortedSetRemove(String keyName, String member) {
        final String fullKeyName = getKeyName(keyName);
        return jedisCluster.zrem(fullKeyName, member);
    }

    public Long sortedSetAdd(String keyName, String member, double value) {
        final String fullKeyName = getKeyName(keyName);
        return jedisCluster.zadd(fullKeyName, value, member);
    }

    public Long delete(String keyName) {
        final String fullKeyName = getKeyName(keyName);
        return jedisCluster.del(fullKeyName);
    }

    public String sortedSetPickRandom(String keyName) {
        final String fullKeyName = getKeyName(keyName);
        final Long size = jedisCluster.zcard(fullKeyName);
        if (size == null || size == 0) {
            return null;
        }

        final int lowerBound = 0;
        final int upperBound = (int)((size > configuration.getCacheRandomSize())
                ? configuration.getCacheRandomSize()
                : size);
        int counter = 0;
        while (counter < configuration.getCacheNumRetries()) {
            final int startRank = (size > MIN_RANDOM_THRESHOLD)
                    ? getRandom(lowerBound, upperBound)
                    : lowerBound;
            final int endRank = startRank + configuration.getCacheMembersPerCall();
            final Set<Tuple> members = jedisCluster.zrevrangeWithScores(fullKeyName, startRank, endRank);
            if (members == null || members.isEmpty()) {
                counter++;
                continue;
            }
            for (Tuple tuple : members) {
                final String pickedMember = tuple.getElement();
                final long numRemoved = jedisCluster.zrem(fullKeyName, pickedMember);
                if (numRemoved > 0) {
                    final String historyKeyName =  getHistoryKeyName(keyName);
                    jedisCluster.zadd(historyKeyName, tuple.getScore(), pickedMember);
                    // extend cache timeout
                    jedisCluster.expire(historyKeyName, configuration.getCacheHistoriesTimeoutInSec());
                    return pickedMember;
                }
                log.info("Collision detected - the member {} has been claimed, looking for the next member...", pickedMember);
            }
            log.info("Collision detected - all members {} have been claimed, trying next...", members.toString());
            counter++;
        }

        // get fresh count of members
        final Long lastestSize = jedisCluster.zcard(fullKeyName);
        log.info("Unable to find a member after {} retries (size = {}) members, giving up...", counter, lastestSize);
        return null;
    }

    /**
     * Picks the top member according to the score in descending order
     *
     * @param keyName
     * @return
     */
    public String sortedSetPickTopOne(String keyName) {
        final String fullKeyName = getKeyName(keyName);
        final Long size = jedisCluster.zcard(fullKeyName);
        if (size == null || size == 0) {
            return null;
        }

        final String pickedMember = sortedSetPickTopWithRetries(fullKeyName, configuration.getCacheNumRetries());
        if (StringUtils.isNotBlank(pickedMember)) {
            final String historyKeyName =  getHistoryKeyName(keyName);
            jedisCluster.zadd(historyKeyName, DEFAULT_SCORE, pickedMember);
            // extend cache timeout
            jedisCluster.expire(historyKeyName, configuration.getCacheHistoriesTimeoutInSec());
        }

        return pickedMember;
    }

    public RedisGlobalLock acquireLock(String name, int timeoutInSec) throws RedisGlobalLockFailException {
        return createLock(name, timeoutInSec);
    }

    public boolean add(String keyName, String value, int timeoutInSec) {
        final String fullKeyName = getKeyName(keyName);
        final String status = jedisCluster.setex(fullKeyName, timeoutInSec, value);
        return isSuccess(status);
    }

    public boolean sadd(String keyName, String value, int timeoutInSec) {
        final String fullKeyName = getKeyName(keyName);
        final Long status = jedisCluster.sadd(fullKeyName, value);
        jedisCluster.expire(fullKeyName, timeoutInSec);
        return status == 1;
    }

    public Set<String> smembers(String keyName) {
        final String fullKeyName = getKeyName(keyName);
        return jedisCluster.smembers(fullKeyName);
    }

    public String spop(String keyName) {
        final String fullKeyName = getKeyName(keyName);
        return jedisCluster.spop(fullKeyName);
    }

    public Long getLockTimeout(String keyName) {
        final String fullKeyName = getLockName(keyName);
        return jedisCluster.ttl(fullKeyName);
    }

    public void setLockTimeout(String keyName, int timeout) {
        final String fullKeyName = getLockName(keyName);
        jedisCluster.expire(fullKeyName, timeout);
    }

    private boolean isSuccess(String status) {
        return STATUS_OK.equals(status);
    }

    public String get(String keyName) {
        final String fullKeyName = getKeyName(keyName);
        return jedisCluster.get(fullKeyName);
    }

    private Long sortedSetAddInternal(String fullKeyName, Map<String, Double> scoreMemberMap, int timeoutInSec) {
        final Long numAdded = jedisCluster.zadd(fullKeyName, scoreMemberMap);
        // extend cache timeout
        jedisCluster.expire(fullKeyName, timeoutInSec);
        return numAdded;
    }

    private Long listPushInternal(String fullKeyName, List<String> data, int timeoutInSec) {
        final String [] values = data.toArray(new String[data.size()]);
        final Long numAdded = jedisCluster.lpush(fullKeyName, values);
        // extend cache timeout
        jedisCluster.expire(fullKeyName, timeoutInSec);
        return numAdded;
    }

    private List<String> listRangeInternal(String fullKeyName, long start, long end) {
        final List<String> list = jedisCluster.lrange(fullKeyName, start, end);
        if(list == null || list.isEmpty()) {
            return Collections.EMPTY_LIST;
        }

        return list;
    }

    private String getKeyName(String key) {
        final String namespace = configuration.getNamespace();
        return String.format(FORMAT_KEY, namespace, key);
    }

    private String getLockName(String key) {
        final String namespace = configuration.getNamespace();
        return String.format(FORMAT_LOCK_KEY, namespace, key);
    }

    private String getHistoryKeyName(String key) {
        final String namespace = configuration.getNamespace();
        return String.format(FORMAT_HISTORY_KEY, namespace, key);
    }

    private String getTempKeyName(String key, String scope) {
        final String namespace = configuration.getNamespace();
        return String.format(FORMAT_TEMP_KEY, namespace, key, scope, UUID.randomUUID());
    }

    protected static int getRandom(int min, int max) {
        final int value = min + random.nextInt(max - min);
        return value;
    }

    private RedisGlobalLock createLock(String name, int timeoutInSec) throws RedisGlobalLockFailException {
        final String fullLockName = getLockName(name);
        final Long value = jedisCluster.incr(fullLockName);
        if (value == null || value == 1) { // acquired the lock
            jedisCluster.expire(fullLockName, timeoutInSec);
            return new RedisGlobalLock(name);
        }
        throw new RedisGlobalLockFailException("Unable to acquire the lock : " + name);
    }

    @Getter
    public class RedisGlobalLock implements AutoCloseable {

        private final String fullLockName;
        private DateTime createdAt;

        private RedisGlobalLock(String name) {
            this.fullLockName = getLockName(name);
            this.createdAt = DateTime.now();
        }

        @Override
        public void close() throws Exception {
            jedisCluster.del(fullLockName);
        }

        public void setTimeout(int timeout) {
            jedisCluster.expire(fullLockName, timeout);
        }

        public long getTimeout() {
            return jedisCluster.ttl(fullLockName);
        }
    }

    private String sortedSetPickTopWithRetries(String fullKeyName, int attempt) {
        if (attempt <= 0) {
            log.error("Unable to find value for cacheKey {} after {} retries", fullKeyName, configuration.getCacheNumRetries());
            return null;
        }

        try (final Jedis jedis = jedisCluster.getJedis(fullKeyName)) {
            final Pipeline pipeline = jedis.pipelined();
            pipeline.multi();
            // get the top member
            Response<Set<String>> response = pipeline.zrevrange(fullKeyName, 0, 0);
            pipeline.zremrangeByRank(fullKeyName, -1, -1);

            pipeline.exec();
            pipeline.sync();
            final Set<String> members = response.get();
            if (members != null && !members.isEmpty()) {
                final String member = members.iterator().next();
                return member;
            }
        }
        catch (Exception ex) {
            log.error("Unable to find value for cacheKey {} ", fullKeyName, ex);
            if (ex instanceof JedisMovedDataException) {
                log.warn("Refreshing slot cache");
                jedisCluster.renewSlotCache();
            }

            return sortedSetPickTopWithRetries(fullKeyName, attempt - 1);
        }

        return null;
    }

    public Long hashDelete(String keyName, String fieldName) {
        final String fullKeyName = getKeyName(keyName);
        final Long retValue = jedisCluster.hdel(fullKeyName, fieldName);
        return retValue;
    }

    public Long hashLength(String keyName) {
        final String fullKeyName = getKeyName(keyName);
        final Long retValue = jedisCluster.hlen(fullKeyName);
        return retValue;
    }

    public Boolean hashExists(String keyName, String fieldName) {
        final String fullKeyName = getKeyName(keyName);
        final Boolean retValue = jedisCluster.hexists(fullKeyName, fieldName);
        return retValue;
    }

    public Long hashIncrement(String keyName, String fieldName, long value, int timeoutInSec) {
        final String fullKeyName = getKeyName(keyName);
        final Long retValue = jedisCluster.hincrBy(fullKeyName, fieldName, value);
        jedisCluster.expire(fullKeyName, timeoutInSec);
        return retValue;
    }

}
