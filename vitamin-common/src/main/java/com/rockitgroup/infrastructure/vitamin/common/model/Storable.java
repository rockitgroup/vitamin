package com.rockitgroup.infrastructure.vitamin.common.model;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter(AccessLevel.PROTECTED)
@ToString
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class Storable implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	public boolean isNew() {
		return id == null;
	}

	public static final Function<Storable, Long> TO_IDENTITY = new Function<Storable, Long>() {
		@Override
		public Long apply(@NonNull Storable input) {
			return input.getId();
		}
	};

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Storable)) {
			return false;
		}
		Storable storable = (Storable) obj;
		return Objects.equal(this.id, storable.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.id);
	}
}
