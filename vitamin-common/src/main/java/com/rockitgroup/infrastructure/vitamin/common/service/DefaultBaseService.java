package com.rockitgroup.infrastructure.vitamin.common.service;

import java.util.List;


public class DefaultBaseService extends BaseService {

    @Override
    protected void clearCache(Object o) {

    }

    @Override
    protected void clearCaches(List list) {

    }
}
