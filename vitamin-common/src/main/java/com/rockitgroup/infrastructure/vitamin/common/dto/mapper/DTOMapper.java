package com.rockitgroup.infrastructure.vitamin.common.dto.mapper;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.rockitgroup.infrastructure.vitamin.common.model.PageResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class DTOMapper {

    private static final String LAZY_LOAD_CLASS_SYMBOL = "_$$_";

    private final MappingHandler<Object, Object> defaultMapperHandler;
    private final Map<String, MappingHandler> mappingHandlerMap = Maps.newHashMap();

    public DTOMapper() {
        this.defaultMapperHandler = new DefaultMappingHandler();
    }

    public <DTO> List<DTO> map(final Iterable<?> models, final Class<DTO> clazz) {
        return Lists.newArrayList(Iterables.transform(models, input -> map(input, clazz)));
    }

    public <DTO> PageResponse<DTO> map(Page<?> pageModel, Class<DTO> clazz) {
        List<DTO> mapedDTOs = map(pageModel.getContent(), clazz);
        return new PageResponse(pageModel.getNumber(), pageModel.getTotalPages(), pageModel.getTotalElements(), mapedDTOs);
    }

    public <DTO> DTO map(Object model, Class<DTO> clazz) {
        MappingHandler handler = getMapper(model.getClass(), clazz);
        return (DTO) handler.map(model, clazz);
    }

    private MappingHandler<?, ?> getMapper(Class<?> modelClass, Class<?> dtoClass) {
        String key = getObjectKey(modelClass, dtoClass);
        return mappingHandlerMap.containsKey(key) ? mappingHandlerMap.get(key) : defaultMapperHandler;
    }

    @Autowired(required = false)
    public void setMappingHandlers(List<MappingHandler> mapperHandlers) {
        Iterables.all(mapperHandlers, input -> {
            setMappingHandler(input);
            return true;
        });
    }

    private void setMappingHandler(MappingHandler mappingHandler) {
        Type type = mappingHandler.getClass().getGenericInterfaces()[0];
        ParameterizedType paramType = (ParameterizedType) type;
        Class<?> modelClass = (Class<?>) paramType.getActualTypeArguments()[0];
        Class<?> dtoClass = (Class<?>) paramType.getActualTypeArguments()[1];

        String key = getObjectKey(modelClass, dtoClass);
        mappingHandlerMap.put(key, mappingHandler);
        log.info("Register MappingHandler : {}", key);
    }

    private String getObjectKey(Class<?> modelClass, Class<?> dtoClass) {
        String modelClassName = modelClass.getName();
        if (modelClassName.contains(LAZY_LOAD_CLASS_SYMBOL)) {
            return modelClassName.substring(0, modelClassName.indexOf(LAZY_LOAD_CLASS_SYMBOL)) + dtoClass.getName();
        } else {
            return modelClass.getName() + dtoClass.getName();
        }
    }

    public interface MappingHandler<MODEL, DTO> {
        DTO map(MODEL model, Class<DTO> clazz);
    }

    static final class DefaultMappingHandler implements MappingHandler<Object, Object> {
        @Override
        public Object map(Object o, Class<Object> clazz) {
            return MapperFacadeFactory.create().map(o, clazz);
        }
    }
}
