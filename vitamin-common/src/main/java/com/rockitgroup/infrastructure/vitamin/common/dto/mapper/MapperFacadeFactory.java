package com.rockitgroup.infrastructure.vitamin.common.dto.mapper;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.builtin.PassThroughConverter;
import ma.glasnost.orika.impl.DefaultMapperFactory;

public final class MapperFacadeFactory {
	private static final MapperFactory FACTORY;

	private MapperFacadeFactory() throws IllegalAccessException {
		throw new IllegalAccessException();
	}

	static {
		FACTORY = new DefaultMapperFactory.Builder().mapNulls(true).useAutoMapping(true).build();
		FACTORY.getConverterFactory().registerConverter(new PassThroughConverter(org.joda.time.DateTime.class));
		FACTORY.getConverterFactory().registerConverter(new OptionalConverter());
	}

	public static MapperFacade create() {
		return FACTORY.getMapperFacade();
	}
}
