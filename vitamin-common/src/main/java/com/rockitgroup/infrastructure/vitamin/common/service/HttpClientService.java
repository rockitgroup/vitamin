package com.rockitgroup.infrastructure.vitamin.common.service;

import com.mysema.commons.lang.URLEncoder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;


@Slf4j
@Component
public class HttpClientService {

    @Value("${httpClient.maxConnectionPerRoute}")
    private int maxConnectionPerRoute;

    @Value("${httpClient.attemptNumber}")
    private int attemptNumber;

    @Value("${httpClient.connectTimeOutInMS}")
    private int connectTimeOutInMS;

    @Value("${httpClient.readTimeOutInMS}")
    private int readTimeOutInMS;

    @Value("${httpClient.requestTimeOutInMS}")
    private int requestTimeOutInMS;

    @Getter
    private HttpClient httpClient;

    @PostConstruct
    private void init() throws Exception {
        httpClient = createHttpClient();
    }

    public HttpResponse get(String url) {
        HttpGet request = new HttpGet(url);
        request.setHeader("Content-Type", "application/json");
        HttpResponse response = null;
        try {
            response = httpClient.execute(request);
        } catch (Exception e) {
            log.error("Unable to get from {}", url, e);
        } finally {
            request.releaseConnection();
        }

        return response;
    }

    public JSONObject getJson(String url) {
        HttpGet request = new HttpGet(url);
        request.setHeader("Content-Type", "application/json");
        HttpResponse response;
        StringBuilder builder = new StringBuilder();
        JSONObject jsonObject = null;
        try {
            response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream inputStream = entity.getContent();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                for (String line; (line = bufferedReader.readLine()) != null; ) {
                    builder.append(line).append("\n");
                }
            }

        } catch (Exception e) {
            log.error("Unable to get json data from {}", url, e);
        } finally {
            request.releaseConnection();
        }

        try {
            jsonObject = new JSONObject(builder.toString());
        } catch (Exception e) {
            log.error("Unable to build json object from {}", builder.toString(), e);
        }
        return jsonObject;
    }

    public JSONObject getJsonWithParameters(String url, List<String> nameValuePare) {
        System.out.println(buildUrLWithParameters(url, nameValuePare));
        HttpGet request = new HttpGet(buildUrLWithParameters(url, nameValuePare));
        request.setHeader("Content-Type", "application/json");
        HttpResponse response;
        StringBuilder builder = new StringBuilder();
        JSONObject jsonObject = null;
        try {
            response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream inputStream = entity.getContent();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                for (String line; (line = bufferedReader.readLine()) != null; ) {
                    builder.append(line).append("\n");
                }
            }

        } catch (Exception e) {
            log.error("Unable to get json data to {}", url, e);
        }
        try {
            jsonObject = new JSONObject(builder.toString());
        } catch (Exception e) {
            log.error("Unable to build json object from {}", builder.toString(), e);
        } finally {
            request.releaseConnection();
        }

        return jsonObject;
    }

    public JSONObject putJson(String url, JSONObject jsonObject) {
        HttpPut putRequest = new HttpPut(url);
        putRequest.addHeader("Content-Type", "application/json");
        putRequest.addHeader("Accept", "application/json");

        StringEntity input;
        HttpResponse response;
        StringBuilder builder = new StringBuilder();

        try {
            input = new StringEntity(jsonObject.toString());
            putRequest.setEntity(input);
            response = httpClient.execute(putRequest);

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream inputStream = entity.getContent();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                for (String line; (line = bufferedReader.readLine()) != null; ) {
                    builder.append(line).append("\n");
                }
            }
            jsonObject = new JSONObject(builder.toString());
        } catch (Exception e) {
            log.error("Unable to put json data to {}", url, e);
        } finally {
            putRequest.releaseConnection();
        }
        return jsonObject;
    }

    public HttpResponse post(String url, JSONObject jsonObject) {
        HttpPost postRequest = new HttpPost(url);
        postRequest.addHeader("Content-Type", "application/json");
        postRequest.addHeader("Accept", "application/json");

        StringEntity input;
        HttpResponse response = null;
        try {
            input = new StringEntity(jsonObject.toString());
            postRequest.setEntity(input);
            response = httpClient.execute(postRequest);
        } catch (Exception e) {
            log.error("Unable to post json data", e);
        } finally {
            postRequest.releaseConnection();
        }
        return response;
    }

    public JSONObject postJson(String url, JSONObject jsonObject) {
        HttpPost postRequest = new HttpPost(url);
        postRequest.addHeader("Content-Type", "application/json");
        postRequest.addHeader("Accept", "application/json");

        StringEntity input;
        HttpResponse response;
        StringBuilder builder = new StringBuilder();

        try {
            input = new StringEntity(jsonObject.toString());
            postRequest.setEntity(input);
            response = httpClient.execute(postRequest);

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream inputStream = entity.getContent();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                for (String line; (line = bufferedReader.readLine()) != null; ) {
                    builder.append(line).append("\n");
                }
            }
            jsonObject = new JSONObject(builder.
                    toString());
        } catch (Exception e) {
            log.error("Unable to post json data to {}", url, e);
        } finally {
            postRequest.releaseConnection();
        }
        return jsonObject;
    }

    public JSONObject postString(String url, String jsonString) {
        HttpPost postRequest = new HttpPost(url);
        postRequest.addHeader("Content-Type", "application/json; charset=utf-8");
        postRequest.addHeader("Accept", "application/json");
        StringEntity input;
        HttpResponse response;
        StringBuilder builder = new StringBuilder();
        JSONObject jsonObject = null;
        try {
            input = new StringEntity(jsonString, "UTF-8");
            postRequest.setEntity(input);
            response = httpClient.execute(postRequest);

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream inputStream = entity.getContent();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                for (String line; (line = bufferedReader.readLine()) != null; ) {
                    builder.append(line).append("\n");
                }
            }
            jsonObject = new JSONObject(builder.toString());
        } catch (Exception e) {
            log.error("Unable to post string data {} to {}", jsonString, url, e);
        } finally {
            postRequest.releaseConnection();
        }
        return jsonObject;
    }


    private String buildUrLWithParameters(String url, List<String> nameValuePairs) {
        StringBuilder sb = new StringBuilder();
        if (nameValuePairs != null && nameValuePairs.size() > 0) {
            for (String nameValue : nameValuePairs) {
                sb.append("&").append(nameValue);
            }
        }

        return url + "?" + URLEncoder.encodeURL(sb.toString());
    }

    private HttpClient createHttpClient() throws Exception {
        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(maxConnectionPerRoute);
        cm.setDefaultMaxPerRoute(maxConnectionPerRoute);

        final DefaultHttpRequestRetryHandler retryHandler = new DefaultHttpRequestRetryHandler(attemptNumber, true);
        final RequestConfig clientConfig = RequestConfig.copy(RequestConfig.DEFAULT)
                .setConnectTimeout(connectTimeOutInMS)
                .setSocketTimeout(readTimeOutInMS)
                .setConnectionRequestTimeout(requestTimeOutInMS)
                .build();

        return HttpClients.custom().setConnectionManager(cm)
                .setRetryHandler(retryHandler)
                .setDefaultRequestConfig(clientConfig)
                .build();
    }
}