package com.rockitgroup.infrastructure.vitamin.common.exception;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 12/23/18
 * Time: 3:40 AM
 */
public class UnauthorizedException extends RuntimeException {

    public UnauthorizedException() {
        super();
    }

    public UnauthorizedException(String s) {
        super(s);
    }

    public UnauthorizedException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnauthorizedException(Throwable cause) {
        super(cause);
    }
}
