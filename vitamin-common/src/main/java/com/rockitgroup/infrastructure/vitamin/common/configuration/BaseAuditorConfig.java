package com.rockitgroup.infrastructure.vitamin.common.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;

//@Configuration
//@EnableJpaAuditing
public abstract class BaseAuditorConfig {

    @Bean
    public AuditorAware auditorProvider() {
        return new AbsentAuditorAware();
    }
}
