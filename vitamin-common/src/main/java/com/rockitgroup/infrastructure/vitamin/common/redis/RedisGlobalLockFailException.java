package com.rockitgroup.infrastructure.vitamin.common.redis;

public class RedisGlobalLockFailException extends Exception {

	public RedisGlobalLockFailException(Exception e) {
		super(e);
	}

	public RedisGlobalLockFailException(String message) {
		super(message);
	}

}
