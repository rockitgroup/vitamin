package com.rockitgroup.infrastructure.vitamin.common.util;

import com.google.common.base.CharMatcher;
import org.apache.commons.lang3.StringUtils;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NumberUtils {

    public static String getRandomNumberCode() {
        // It will generate 6 digit random Number.
        // from 0 to 999999
        Random rnd = new Random();
        int number = rnd.nextInt(999999);

        // this will convert any number sequence into 6 character.
        return String.format("%06d", number);
    }

    public static Double findDoubleFromString(String input) {
        if (input == null) {
            return 0.0;
        }
        Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");
        Matcher matcher = pattern.matcher(input);
        if (matcher.find()) {
            return Double.parseDouble(matcher.group());
        }
        return 0.0;
    }

    public static Integer findIntFromString(String input) {
        if (input == null) {
            return 0;
        }
        String output = CharMatcher.digit().retainFrom(input);
        if (StringUtils.isNotEmpty(output)) {
            return Integer.parseInt(output);
        }
        return 0;
    }
}
