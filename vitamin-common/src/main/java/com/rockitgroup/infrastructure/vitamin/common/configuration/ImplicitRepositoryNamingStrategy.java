package com.rockitgroup.infrastructure.vitamin.common.configuration;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.atteo.evo.inflector.English;
import org.hibernate.HibernateException;
import org.hibernate.boot.model.naming.*;
import org.hibernate.cfg.ImprovedNamingStrategy;

@Slf4j
public class ImplicitRepositoryNamingStrategy extends ImplicitNamingStrategyJpaCompliantImpl {

    private static final ImprovedNamingStrategy STRATEGY_INSTANCE = new ImprovedNamingStrategy();

    @Override
    public Identifier determinePrimaryTableName(ImplicitEntityNameSource source) {
        if ( source == null ) {
            // should never happen, but to be defensive...
            throw new HibernateException( "Entity naming information was not provided." );
        }

        String tableName = transformEntityName( source.getEntityNaming() );

        if ( tableName == null ) {
            // todo : add info to error message - but how to know what to write since we failed to interpret the naming source
            throw new HibernateException( "Could not determine primary table name for entity" );
        }

        return toIdentifier(STRATEGY_INSTANCE.classToTableName(transformToPluralForm(tableName)), source.getBuildingContext() );
    }

    @Override
    public Identifier determineJoinTableName(ImplicitJoinTableNameSource source) {
        // JPA states we should use the following as default:
        //		"The concatenated names of the two associated primary entity tables (owning side
        //		first), separated by an underscore."
        // aka:
        // 		{OWNING SIDE PRIMARY TABLE NAME}_{NON-OWNING SIDE PRIMARY TABLE NAME}
        final String entityName = transformEntityName( source.getOwningEntityNaming() );

        final String name = entityName
                + "_"
                + source.getNonOwningPhysicalTableName();

        return toIdentifier(STRATEGY_INSTANCE.classToTableName(name), source.getBuildingContext() );
    }

    @Override
    public Identifier determineCollectionTableName(ImplicitCollectionTableNameSource source) {
        // JPA states we should use the following as default:
        //      "The concatenation of the name of the containing entity and the name of the
        //       collection attribute, separated by an underscore.
        // aka:
        //     if owning entity has a JPA entity name: {OWNER JPA ENTITY NAME}_{COLLECTION ATTRIBUTE NAME}
        //     otherwise: {OWNER ENTITY NAME}_{COLLECTION ATTRIBUTE NAME}
        final String entityName = transformEntityName( source.getOwningEntityNaming() );

        final String name = entityName
                + "_"
                + transformAttributePath( source.getOwningAttributePath() );

        return toIdentifier( STRATEGY_INSTANCE.classToTableName(name), source.getBuildingContext() );
    }

    @Override
    public Identifier determineJoinColumnName(ImplicitJoinColumnNameSource source) {
        // JPA states we should use the following as default:
        //
        //	(1) if there is a "referencing relationship property":
        //		"The concatenation of the following: the name of the referencing relationship
        // 			property or field of the referencing entity or embeddable class; "_"; the
        // 			name of the referenced primary key column."
        //
        //	(2) if there is no such "referencing relationship property", or if the association is
        // 			an element collection:
        //     "The concatenation of the following: the name of the entity; "_"; the name of the
        // 			referenced primary key column"

        // todo : we need to better account for "referencing relationship property"

        final String name;

        if ( source.getNature() == ImplicitJoinColumnNameSource.Nature.ELEMENT_COLLECTION
                || source.getAttributePath() == null ) {
            name = transformEntityName( source.getEntityNaming() )
                    + StringUtils.capitalize(source.getReferencedColumnName().getText());
        }
        else {
            name = transformAttributePath( source.getAttributePath() )
                    + StringUtils.capitalize(source.getReferencedColumnName().getText());
        }

        return toIdentifier( name, source.getBuildingContext() );
    }

    private String transformToPluralForm(String tableNameInSingularForm) {
        return English.plural(tableNameInSingularForm, 2);
    }
}
