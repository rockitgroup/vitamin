package com.rockitgroup.infrastructure.vitamin.common.service;

import com.rockitgroup.infrastructure.vitamin.common.model.Deletable;
import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import com.rockitgroup.infrastructure.vitamin.common.repository.BaseRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


public abstract class BaseService {

    protected BaseRepository repository;

    @Transactional
    public Object save(Modifiable object) {
        clearCache(object);
        return repository.saveAndFlush(object);
    }

    @Transactional
    public List save(List objects) {
        clearCaches(objects);
        return repository.saveAll(objects);
    }

    public void delete(Deletable object) {
        clearCache(object);
        object.setDeleted(true);
        save(object);
    }

    @Async
    @Transactional
    public void saveAsync(Modifiable object) {
        clearCache(object);
        repository.saveAndFlush(object);
    }

    @Async
    @Transactional
    public void saveAsync(List objects) {
        clearCaches(objects);
        repository.saveAll(objects);
    }

    public Optional findOneById(Long id) {
        return repository.findById(id);
    }

    protected abstract void clearCache(Object object);

    protected abstract void clearCaches(List objects);
}
