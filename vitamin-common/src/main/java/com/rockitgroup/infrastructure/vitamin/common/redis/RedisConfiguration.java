package com.rockitgroup.infrastructure.vitamin.common.redis;

import com.google.common.collect.Sets;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.HostAndPort;

import java.util.Set;

@Configuration
public class RedisConfiguration {

    public static final String SEP_COMMA = ",";
    public static final String SEP_COLON = ":";

    @Value("${redis.cluster}")
    private String cluster;

    @Value("${redis.default.port:6379}")
    private Integer defaultPort;

    @Value("${redis.namespace:prod}")
    @Getter private String namespace;

    @Value("${redis.connection.timeout.in.ms}")
    @Getter private Integer connectionTimeoutInMS;

    @Value("${redis_pool.config.max.total}")
    @Getter private Integer poolConfigMaxTotal;

    @Value("${redis.pool.config.max.idle}")
    @Getter private Integer poolConfigMaxIdle;

    @Value("${redis.pool.config.min.idle}")
    @Getter private Integer poolConfigMinIdle;

    @Value("${redis.pool.config.test.on.borrow}")
    @Getter private Boolean poolConfigTestOnBorrow;

    @Value("${redis.pool.config.test.on.return}")
    @Getter private Boolean poolConfigTestOnReturn;

    @Value("${redis.pool.config.test.while.idle}")
    @Getter private Boolean poolConfigTestWhileIdle;

    @Value("${redis.pool.config.min.evictable.idle.time.ms}")
    @Getter private Integer poolConfigMinEvictableIdleTimeInMS;

    @Value("${redis.pool.config.time.between.eviction.runs.ms}")
    @Getter private Integer poolConfigTimeBetweenEvictionRunsInMS;

    @Value("${redis.pool.config.num.tests.per.eviction.run}")
    @Getter private Integer poolConfigNumTestsPerEvictionRun;

    @Value("${redis.pool.config.block.when.exhausted}")
    @Getter private Boolean poolConfigBlockWhenExhausted;

    @Value("${redis.cache.histories.timeout.sec}")
    @Getter private Integer cacheHistoriesTimeoutInSec;

    @Value("${redis.cache.random.size}")
    @Getter private Integer cacheRandomSize;

    @Value("${redis.cache.num.retries}")
    @Getter private Integer cacheNumRetries;

    @Value("${redis.cache.members.per.call}")
    @Getter private Integer cacheMembersPerCall;

    public Set<HostAndPort> getClusterHostAndPorts() {
        Set<HostAndPort> hostAndPorts = Sets.newHashSet();
        String [] tokens = cluster.split(SEP_COMMA);
        for (String server : tokens) {
            final String [] hostParts = server.split(SEP_COLON);
            String host = server;
            int port = defaultPort;
            if (hostParts.length > 1) {
                host = hostParts[0];
                port = Integer.valueOf(hostParts[1]);
            }
            HostAndPort hostAndPort = new HostAndPort(host, port);
            hostAndPorts.add(hostAndPort);
        }

        return hostAndPorts;
    }

}
