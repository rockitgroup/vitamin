package com.rockitgroup.infrastructure.vitamin.common.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.MappedSuperclass;

@Getter
@Setter
@ToString(callSuper = true)
@MappedSuperclass
public class Deletable extends Modifiable {

    private boolean deleted;
}
