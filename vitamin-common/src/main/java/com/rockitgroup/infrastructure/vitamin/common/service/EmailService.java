package com.rockitgroup.infrastructure.vitamin.common.service;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class EmailService {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private Configuration freeMarkerConfig;

    @Async
    public void sendEmailsAsync(InternetAddress fromAddress, List<String> toEmails, String subject, String emailTemplate, Map model, boolean isHtml) throws MessagingException, IOException, TemplateException {
        sendEmails(fromAddress, toEmails, subject, emailTemplate, model, isHtml);
    }

    public void sendEmails(InternetAddress fromAddress, List<String> toEmails, String subject, String emailTemplate, Map model, boolean isHtml) throws MessagingException, IOException, TemplateException {
        String finalTemplatePath = (!emailTemplate.startsWith("email")) ? "email/" + emailTemplate : emailTemplate;
        Template template = freeMarkerConfig.getTemplate(finalTemplatePath);
        String html = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());

        String[] sendToEmails = new String[toEmails.size()];
        sendToEmails = toEmails.toArray(sendToEmails);

        helper.setFrom(fromAddress);
        helper.setTo(sendToEmails);
        helper.setSubject(subject);
        helper.setText(html, isHtml);

        mailSender.send(message);
    }

}
