package com.rockitgroup.infrastructure.vitamin.common.configuration;

import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

public class AbsentAuditorAware implements AuditorAware {

    @Override
    public Optional getCurrentAuditor() {
        return Optional.empty();
    }

}
