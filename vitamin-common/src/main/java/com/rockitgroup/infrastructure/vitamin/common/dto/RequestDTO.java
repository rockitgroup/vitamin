package com.rockitgroup.infrastructure.vitamin.common.dto;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 9/20/18
 * Time: 2:50 PM
 */
public abstract class RequestDTO {

    public abstract void validate() throws IllegalArgumentException;
}
