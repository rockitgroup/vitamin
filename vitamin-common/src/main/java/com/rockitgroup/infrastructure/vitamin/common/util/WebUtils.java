package com.rockitgroup.infrastructure.vitamin.common.util;

import com.rockitgroup.infrastructure.vitamin.common.dto.RequestDTO;
import com.rockitgroup.infrastructure.vitamin.common.dto.ResponseDTO;
import com.rockitgroup.infrastructure.vitamin.common.exception.BadRequestException;
import com.rockitgroup.infrastructure.vitamin.common.exception.NotFoundException;
import com.rockitgroup.infrastructure.vitamin.common.exception.UnauthorizedException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Map;


public class WebUtils extends org.springframework.web.util.WebUtils {

    public static final String DEFAULT_CHARSET = "utf-8";

    public static final String USER_AGENT_HEADER_ATTR_NAME = "User-Agent";

    public static final String CONTENT_DISPOSITION_HEADER_ATTR_NAME = "Content-Disposition";
    public static final int DEFAULT_HTTP_PORT = 80;
    public static final int DEFAULT_HTTPS_PORT = 443;

    public static Long getAccountIdFromAuthorizationValue(String authorization) {
        if (StringUtils.isNotEmpty(authorization)) {
            String[] authorizationData = authorization.split(":");
            if (authorizationData.length == 2) {
                return Long.parseLong(authorizationData[0]);
            }
        }
        return null;
    }

    public static ResponseDTO authorizedPost(String url, String authorization) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", authorization);
        headers.add("Content-Type", "application/json");

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        HttpEntity authRequest = new HttpEntity(headers);

        return restTemplate.postForObject(url, authRequest, ResponseDTO.class);
    }

    public static ResponseDTO authorizedPost(String url, String authorization, RequestDTO requestDTO) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", authorization);
        headers.add("Content-Type", "application/json");

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        HttpEntity<RequestDTO> authRequest = new HttpEntity<>(requestDTO, headers);

        return restTemplate.postForObject(url, authRequest, ResponseDTO.class);
    }

    public static ResponseDTO authorizedGet(String url, String authorization) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", authorization);
        headers.set("Content-Type", "application/json");

        HttpEntity authRequest = new HttpEntity(headers);

        ResponseEntity<ResponseDTO> response = restTemplate.exchange(url, HttpMethod.GET, authRequest, ResponseDTO.class);
        return response.getBody();
    }

    public static ResponseDTO authorizedGet(String url, String authorization, Map<String, String> param) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", authorization);
        headers.set("Content-Type", "application/json");

        HttpEntity authRequest = new HttpEntity(headers);

        ResponseEntity<ResponseDTO> response = restTemplate.exchange(url, HttpMethod.GET, authRequest, ResponseDTO.class, param);
        return response.getBody();
    }

    public static String getRequestURLWithQueryString(HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        String queryString = (request.getQueryString() != null) ? "?" + request.getQueryString() : "";
        return requestURI + queryString;
    }

    public static String getRequestAddress(HttpServletRequest request) {
        String requestAddress = request.getScheme() + "://" + request.getServerName()
                + ((request.getServerPort() == DEFAULT_HTTP_PORT || request.getServerPort() == DEFAULT_HTTPS_PORT) ? "" : ":" + request.getServerPort())
                + request.getContextPath();
        return requestAddress;
    }

    public static Cookie createCookie(String name, String value) {
        return createCookie(name, value, null, null, -1);
    }

    public static Cookie createCookie(String name, String value, int maxAge) {
        return createCookie(name, value, null, null, maxAge);
    }

    public static Cookie createCookie(String name, String value, String path, int maxAge) {
        return createCookie(name, value, null, path, maxAge);
    }

    public static Cookie createCookie(String name, String value, String domain, String path, int maxAge) {
        Cookie cookie = new Cookie(name, encode(value));

        if (domain != null) {
            cookie.setDomain(domain);
        }
        if (path != null) {
            cookie.setPath(path);
        }
        cookie.setMaxAge(maxAge);

        return cookie;
    }

    public static String getCookieValue(HttpServletRequest request, String name) {
        Cookie cookie = getCookie(request, name);
        return cookie != null ? decode(cookie.getValue()) : null;
    }

    public static boolean existCookie(HttpServletRequest request, String name) {
        return getCookie(request, name) != null;
    }

    public static String encode(String value) {
        if (value == null) {
            return null;
        }

        try {
            return URLEncoder.encode(value, DEFAULT_CHARSET);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    public static String decode(String value) {
        try {
            return URLDecoder.decode(value, DEFAULT_CHARSET);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    public static void setDownloadFileName(HttpServletRequest request, HttpServletResponse response, String fileName) {
        String userAgent = request.getHeader(USER_AGENT_HEADER_ATTR_NAME);

        try {
            if (StringUtils.contains(userAgent, "MSIE")) {
                response.setHeader(CONTENT_DISPOSITION_HEADER_ATTR_NAME, "attachment;filename=" + URLEncoder.encode(fileName, DEFAULT_CHARSET) + ";");
            } else {
                response.setHeader(CONTENT_DISPOSITION_HEADER_ATTR_NAME, "attachment;filename="
                        + new String(fileName.getBytes(DEFAULT_CHARSET), "latin1") + ";");
            }

        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("파일명 지정 실패 - " + e.getMessage(), e);
        }

        response.setHeader("Pragma", "no-cache;");
        response.setHeader("Expires", "-1;");
    }

    public static void handleAPIException(ResponseDTO responseDTO) throws Exception {
        if (responseDTO.getStatus() == HttpStatus.UNAUTHORIZED.value()) {
            throw new UnauthorizedException(responseDTO.getError().getMessage());
        } else if (responseDTO.getStatus() == HttpStatus.NOT_FOUND.value()) {
            throw new NotFoundException(responseDTO.getError().getMessage());
        } else if (responseDTO.getStatus() == HttpStatus.BAD_REQUEST.value()) {
            throw new BadRequestException(responseDTO.getError().getMessage());
        } else {
            throw new Exception(responseDTO.getError().getMessage());
        }
    }
}
